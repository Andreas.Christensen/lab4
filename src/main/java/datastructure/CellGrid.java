package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private int rows;
	private int cols;
	private CellState [][] cellStates;

    public CellGrid(int rows, int columns, CellState initialState) {
    	this.rows = rows;
    	this.cols = columns;
    	this.cellStates = new CellState[rows][cols];
    	for (int i = 0; i < rows; i++) {
    		for (int j = 0; j < columns; j++) {
    			cellStates[i][j] = initialState;
    		}
    	}
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }
    
    private void checkBounds(int row, int column) {
    	if ((row < 0 || row >= numRows()) || (column < 0 || column >= numColumns())) {
    		throw new IndexOutOfBoundsException();
    	}
    }

    @Override
    public void set(int row, int column, CellState element) {
    	checkBounds(row, column);
    	cellStates[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
    	checkBounds(row, column);
        return cellStates[row][column];
    }
    

    @Override
    public IGrid copy() {
    	CellGrid gridCopy = new CellGrid(this.rows, this.cols, null);
    	for(int i = 0; i < rows; i++) {
    		for (int j = 0; j < cols; j++) {
    			gridCopy.set(i, j, this.get(i, j));
    		}
    	}
        return gridCopy;
    }
    
}
